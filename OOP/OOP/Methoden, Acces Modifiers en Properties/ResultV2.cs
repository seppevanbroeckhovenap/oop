﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
    class ResultV2
    {
        public byte Percentage { get; set; }

        public  Honors ComputeHonors()
        {
            if (Percentage < 50)
            {
                return Honors.NietGeslaagd;
            }
            else if (Percentage <= 68)
            {
                return Honors.Voldoende;
            }

            else if (Percentage <= 75)
            {
                return Honors.Onderscheiding;
            }
            else if (Percentage <= 85)
            {
                return Honors.GrooteOnderscheiding;
            }

            else 
            {
                return Honors.GrootsteOnderscheiding;
            }
        }

        public static void Main()
        {
            ResultV2 result1 = new ResultV2();
            result1.Percentage = 40;
            ResultV2 result2 = new ResultV2();
            result2.Percentage = 60;
            ResultV2 result3 = new ResultV2();
            result3.Percentage = 80;
            ResultV2 result4 = new ResultV2();
            result4.Percentage = 90;

            Console.WriteLine(result1.ComputeHonors());
            Console.WriteLine(result2.ComputeHonors());
            Console.WriteLine(result3.ComputeHonors());
            Console.WriteLine(result4.ComputeHonors());
        }
   
	}
}

        
    
    

