﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
    class Rectangle
    {
		private int height = 1;

		public int Height
		{
			get { return height; }
			set
			{
				if (height <=0)
				{
					Console.WriteLine($"Het is verboden een hoogte van {height} in te stellen");
				}
				else
				{
					height = value;
				}
			}
			
		}

		private int width = 1;

		public int Width
		{
			get { return width; }
			set {
				if (width<=0)
				{
					Console.WriteLine($"Het is verboden een hoogte van {width} in te stellen");
				}
				else
				{
					width = value;
				}
			}
			
		}

		private double surface;

		public double Surface
		{
			get { return surface = Width * Height; }
			
		}

	}
}
