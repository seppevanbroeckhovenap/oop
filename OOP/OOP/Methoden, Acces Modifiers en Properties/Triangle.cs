﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
    class Triangle
    {
		private int _base;

		public int Base
		{
			get { return _base; }
			set
			{
				if (_base<0)
				{
					Console.WriteLine($"Het is verboden een hoogte van {_base} in te stellen");
				}
				else
				{
					_base = value;
				}
			}
				
		}


		private int height;

		public int Height
		{
			get { return height; }
			set
			{
				if (height < 0)
				{
					Console.WriteLine($"Het is verboden een hoogte van {height} in te stellen");
				}
				else
				{
					height = value;
				}
			}

		}

		private int surface;

		public int Surface
		{
			get { return surface = height*_base/2; }
			
		}

	}
}
