﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
    class MethodenAccesModifiersProperties
    {
        public static void StartSubmenu()
        {
            Console.Clear();

            Console.WriteLine("Welke oefening wil je uitvoeren?");

            Console.WriteLine("1. Printen resultaten V1 (H8-RapportModule-V1)");
            Console.WriteLine("2. Printen resultaten V2 (H8-RapportModule-V2)");
            Console.WriteLine("3. Getallencombinaties maken (H8-Getallencombinatie)");
            Console.WriteLine("4. Berekenen van oppervlaktes (H8-Figuren)");

            int choice = int.Parse(Console.ReadLine());


            switch (choice)
            {
                case 1:
                    Console.Clear();
                    ResultV1.Main();
                    break;
                case 2:
                    Console.Clear();
                    ResultV2.Main();
                    break;
                case 3:
                    Console.Clear();
                    NumberCombination.Main();
                    break;
                case 4:
                    Console.Clear();
                    FigureProgram.Main();
                    break;

                default:
                    Console.WriteLine("Ongeldige keuze.");
                    break;

            }
        }
    }
}

