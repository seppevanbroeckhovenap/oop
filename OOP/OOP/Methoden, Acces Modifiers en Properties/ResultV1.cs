﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
    class ResultV1
    {

        public byte Percentage { get; set; }
       

        public  static string printHonours(byte result)
        {
            string text = "";
            if (result < 50)
            {
                text = "niet geslaagd";
            }
            else if (result <= 68)
            {
                text = "voldoende";
            }
            else if (result <= 75)           
            {
                text = "onderscheiding";
            }
            else if (result <= 85)
            {
                text = "grote onderscheiding";
            }

            else
            {
                text = "grootste onderscheiding";
            }
            return text;
        }
        public static void Main()

        {
            ResultV1 result1 = new ResultV1();
            result1.Percentage = 40;
            ResultV1 result2 = new ResultV1();
            result2.Percentage = 60;
            ResultV1 result3 = new ResultV1();
            result3.Percentage = 80;
            ResultV1 result4 = new ResultV1();
            result4.Percentage = 90;

            Console.WriteLine(printHonours(result1.Percentage));
            Console.WriteLine(printHonours(result2.Percentage));
            Console.WriteLine(printHonours(result3.Percentage));
            Console.WriteLine(printHonours(result4.Percentage));
        }
    }
}
