﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP.Geometry
{
    class ShapesBuilder
    {
        private ConsoleColor color;


		public ConsoleColor Color
		{
			get { return color; }
			set
			{
				color = value;
				Console.ForegroundColor = color;
			}
		}
		

		public char symbol
		{
			get { return '-'; }
			set { symbol = value; }
		}

		public string Line (int length, char alternateSymbol, ConsoleColor color)
		{

			Console.ForegroundColor = color;
			return new string(alternateSymbol, length);
		}

		public string Line( int lenght)
		{
			return new string(symbol, lenght);
		}

		public string Line(int lenght, char alternateSymbol)
		{
			return new string(alternateSymbol, lenght);
		}

		public string Line (int length, ConsoleColor color)
		{
			Console.ForegroundColor = color;
			return new string(symbol, length);
		}
		
		public string Rectangle (int height, int width)
		{
			return Rectangle(height, width, symbol);
		}
		public string Rectangle(int height, int width, char alternateSymbol)
		{
			string output = "";

			for (int i = 0; i < height; i++)
			{
				output += Line(width, alternateSymbol) + "\n";
						
			}

			return output;

		}
		public string Rectangle(int height, int width, ConsoleColor color)
		{
			string output = "";

			for (int i = 0; i < height; i++)
			{
				output += Line(width, color) + "\n";

			}

			return output;

		}

		public string Triangle (int height,  char alternateSymbol)
		{
			string output = "";
			for (int i = 0; i <= height; i++)
			{
				output += Line(i, alternateSymbol) + "\n";
					
			}
			return output;
		}
		public string Triangle(int height)
		{
			return Triangle(height, symbol);
		}
	}
}
