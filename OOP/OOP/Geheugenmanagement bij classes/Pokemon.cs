﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
    class Pokemon
    {
		private int maxHP;

		public int MaxHP
		{
			get { return maxHP; }
			set {
				if (value<20)
				{
					maxHP = 20;
				}
				else if (value>1000)
				{
					maxHP = 1000;
				}
				else
				{
					maxHP = value;
				}
				}

		
		}
		private int hp;

		public int HP
		{
			get { return hp; }
			set
			{
				if (value <0)
				{
					hp = 0;
				}
				else if (value>MaxHP)
				{
					hp = MaxHP;
				}
				else
				{
					hp = value;
				}
				
				
				 }


		}
		public PokeSpecies PokeSpecies { get; set; }

		public PokeTypes PokeType { get; set; }


		public void Attack()
		{
			switch (PokeType)
			{
				case PokeTypes.Grass:
					Console.ForegroundColor = ConsoleColor.Green;
					break;
				case PokeTypes.Fire:
					Console.ForegroundColor = ConsoleColor.Red;
					break;
				case PokeTypes.Water:
					Console.ForegroundColor = ConsoleColor.Blue;
					break;
				case PokeTypes.Electric:
					Console.ForegroundColor = ConsoleColor.Yellow;
					break;
				default:

					
					break;
					
			}
			Console.WriteLine(PokeSpecies.ToString().ToUpper() + "!");
			Console.ResetColor();
		}
		public static void MakePokemon()
		{
			Pokemon bulbasaur = new Pokemon();
			bulbasaur.MaxHP = 20;
			bulbasaur.HP = 20;
			bulbasaur.PokeSpecies = PokeSpecies.Bulbasaur;
			bulbasaur.PokeType = PokeTypes.Grass;
			Pokemon charmander = new Pokemon();
			charmander.MaxHP = 20;
			charmander.HP = 20;
			charmander.PokeSpecies = PokeSpecies.Charmander;
			charmander.PokeType = PokeTypes.Fire;
			Pokemon squirtle = new Pokemon();
			squirtle.MaxHP = 20;
			squirtle.HP = 20;
			squirtle.PokeSpecies = PokeSpecies.Squirtle;
			squirtle.PokeType = PokeTypes.Water;
			Pokemon pikachu = new Pokemon();
			pikachu.MaxHP = 20;
			pikachu.HP = 20;
			pikachu.PokeSpecies = PokeSpecies.Pikachu;
			pikachu.PokeType = PokeTypes.Electric;
			bulbasaur.Attack();
			charmander.Attack();
			squirtle.Attack();
			pikachu.Attack();
		}

		public static Pokemon FirstConsciousPokemon(Pokemon[] pokemons)
			{

			foreach (Pokemon somePokemon in pokemons)
			{
				if (somePokemon.HP > 0)
				{
					return somePokemon;
				}
			}
			return null;

		}


	}
}
