﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
    class GeheugenmanagementBijClasses
    {
        public static void StartSubmenu()
        {
            Console.Clear();

            Console.WriteLine("Welke oefening wil je uitvoeren?");

            Console.WriteLine("1. Pokemon (H9-pokeattack)");
     ;
            int choice = int.Parse(Console.ReadLine());


            switch (choice)
            {

                case 1:
                    Console.Clear();
                    Pokemon.MakePokemon();
                    break;

                default:
                    Console.WriteLine("Ongeldige keuze.");
                    break;

            }
        }
    }
}
