﻿using OOP.Geometry;
using System;

namespace OOP
{
    class Program
    {
        static void Main(string[] args)
        {
            while (true)
            {
                Console.WriteLine();

                Console.WriteLine("\r\r\r");

                Console.WriteLine("Van welk hoofstuk wil je oefeningen laten uitvoeren?");
                Console.WriteLine("1. Smaakmakers OOP");
                Console.WriteLine("2. Leren werken met DateTime objecten(H8-Klassen en Objecten");
                Console.WriteLine("3. Methoden, Acces Modifiers en Properties (H8-Klassen en Objecten");
                Console.WriteLine("4. GeheugenManagement bij klasses (H9)");
                Console.WriteLine("5. Geavanceerde klassen en objecten (H10)");
                Console.Write("\rKeuze: ");


                int choice = int.Parse(Console.ReadLine());
                switch (choice)
                {
                    case 1:
                        SmaakmakerOOP.StartSubmenu();
                        break;
                    case 2:
                        LerenWerkenMetDateTIme.StartSubmenu();
                        break;
                    case 3:
                        MethodenAccesModifiersProperties.StartSubmenu();
                        break;
                    case 4:
                        GeheugenmanagementBijClasses.StartSubmenu();
                        break;
                 


                    default:
                        Console.WriteLine("Ongeldige keuze!");
                        break;

                }
            }
        }
    }
}
