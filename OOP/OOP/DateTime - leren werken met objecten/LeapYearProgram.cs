﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
    class LeapYearProgram
    {
        public static void Main()
        {
            int counter = 0;

            for (int i = 1800; i < 2020; i++)
            {
                if (DateTime.IsLeapYear(i))
                    counter++;
            }

            Console.WriteLine($"Tussen 1800 en 2020 waren er {counter.ToString()} schrikkeljaren");
        }
    }
}
