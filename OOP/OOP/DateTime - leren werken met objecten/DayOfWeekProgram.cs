﻿using System;
using System.Globalization;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
    class DayOfWeekProgram
    {

       public static void Main()
        {
            Console.WriteLine("Welke dag?");
            int day = int.Parse(Console.ReadLine());

            Console.WriteLine("Welke maand?");
            int month = int.Parse(Console.ReadLine());

            Console.WriteLine("Welk jaar?");
            int  year = int.Parse(Console.ReadLine());

            DateTime date = new DateTime(year, day, month);

            CultureInfo belgianCI = new CultureInfo("nl-BE");


            Console.WriteLine($"{date.ToString("dddd mm YYYY")} ");
        }
    }
}
