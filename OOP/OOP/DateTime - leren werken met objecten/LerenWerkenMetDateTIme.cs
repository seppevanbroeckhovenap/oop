﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{ 
    class LerenWerkenMetDateTIme
    {
    public static void StartSubmenu()
    {
        Console.WriteLine();

        Console.WriteLine("Wat wil je doen");
        Console.WriteLine("1. Oefening Datetime");
        Console.WriteLine("2. Ticks2000program");
        Console.WriteLine("3. LeapYearProgram");
        Console.WriteLine("4. ArrayTimerProgram");
        int choise = int.Parse(Console.ReadLine());
        switch (choise)
        {
            case 1:
                DayOfWeekProgram.Main();
                break;
            case 2:
                Ticks2000program.Main();
                break;
            case 3:
                LeapYearProgram.Main();
                break;
            case 4:
                ArrayTimerProgram.Main();
                break;

                default:
                    Console.WriteLine("Geef een gelidg getal op");
                    break;
        }
    }
    }
}
