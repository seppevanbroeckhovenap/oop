﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
    class Ticks2000program
    {
        public static void Main()
    
    {
        DateTime year2000 = new DateTime(2000, 1, 1);
        DateTime now = DateTime.Now;

            TimeSpan difference = now - year2000;
            Console.WriteLine($"Sinds het jaar 2000 waren er {difference.Ticks.ToString()} ticks" );
    }
}


}
