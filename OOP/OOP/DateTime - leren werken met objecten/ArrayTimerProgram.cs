﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
    class ArrayTimerProgram
    {
        public  static void Main()
        {

            DateTime start =  DateTime.Now;
            int[] million = new int[100000];

            for (int i = 0; i < million.Length; i++)
            {
                million[i] = 1 + i;
                
            }

            DateTime end = DateTime.Now;
            TimeSpan timeSpan = end - start;

            Console.WriteLine($"Het duurt {timeSpan.Milliseconds.ToString()} milliseconden");

        }
    }
}
